/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mfaiz
 */
public class PlayerIT {

    Dice d1, d2, d3;
    Game game;

    public PlayerIT() {
    }

    @Before
    public void setUp() {
        d1 = new Dice();
        d2 = new Dice();
        d3 = new Dice();

        game = new Game(d1, d2, d3);
    }

    @After
    public void tearDown() {
        d1 = null;
        d2 = null;
        d3 = null;
    }

    @Test
    public void testBettingLimit() {
        int balance = 100;
        int limit = 0;
        Player player = new Player("Vikram", balance);
        player.setLimit(limit);
        
        int bet = 5;
        while (player.balanceExceedsLimitBy(bet) && player.getBalance() < 200) {
            DiceValue pick = DiceValue.getRandom();
            game.playRound(player, pick, bet);
        }
        
        int expResult = 0;
        assertEquals(expResult, player.getBalance());
    }
    
    @Test
    public void testbalanceExceedsLimitBy() {
        int balance = 5;
        int limit = 0;
        Player player = new Player("Vikram", balance);
        player.setLimit(limit);
        
        int bet = 5;
        
        Boolean expResult = true;
        assertEquals(expResult, player.balanceExceedsLimitBy(bet));
    }
}
