/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class GameIT {

    Dice d1, d2, d3;
    Game game;

    public GameIT() {
    }

    @Before
    public void setUp() {
        d1 = new Dice();
        d2 = new Dice();
        d3 = new Dice();

        game = new Game(d1, d2, d3);
    }

    @After
    public void tearDown() {
        d1 = null;
        d2 = null;
        d3 = null;
    }

    @Test
    public void testWinningBets() {
        System.out.println("Test Winning Bets");        
        int bet = 5;

        int winnings = 0;
        ///If winnings is 0 then run again to check for winnnigs > 0
        while (winnings == 0) {
            int balance = 100;
            int limit = 0;
            Player player = new Player("Vikram", balance);
            player.setLimit(limit);
            
            DiceValue pick = DiceValue.getRandom();

            winnings = game.playRound(player, pick, bet);
            if (winnings > 0) {
                int expResult = balance + winnings;
                assertEquals(expResult, player.getBalance());
            } else {
                int expResult = balance - bet;
                balance -= bet;
                assertEquals(expResult, player.getBalance());
            }
        }
    }

    @Test
    public void testGameWinRatioOdd() {
        System.out.println("Test Game Win Ratio Odd");

        Player player;

        int winCount = 0;
        int loseCount = 0;
        for (int i = 0; i < 10; i++) {
            String name = "Vikram";
            int balance = 100;
            int limit = 0;
            player = new Player(name, balance);
            player.setLimit(limit);
            int bet = 5;

            while (player.balanceExceedsLimitBy(bet) && player.getBalance() < 200) {
                DiceValue pick = DiceValue.getRandom();
                int winnings = game.playRound(player, pick, bet);

                if (winnings > 0) {
                    winCount++;
                } else {
                    loseCount++;
                }
            }
        }

        float winRatio = (float) winCount / (winCount + loseCount);
        assertEquals(0.42, winRatio, 0.01);
    }

}
