/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mfaiz
 */
public class DiceValueIT {
    
    public DiceValueIT() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetRandom() {
        System.out.println("DiceValue getRandom");
        
        Boolean isSpadeGenerated = false;        
        for(int i=0; i < 1000; i++){
            DiceValue result = DiceValue.getRandom();
            if(result == DiceValue.SPADE){
                isSpadeGenerated = true;
                break;
            }            
        }
        Boolean expResult = true;        
        assertEquals(expResult, isSpadeGenerated);
    }
    
}
