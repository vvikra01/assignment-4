/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mfaiz
 */
public class DiceIT {
    Dice d1;
    
    public DiceIT() {
    }
    
    @Before
    public void setUp() {
        d1 = new Dice();
    }
    
    @After
    public void tearDown() {
        d1 = null;
    }

    @Test
    public void testRoll() {
        DiceValue rolled = d1.roll();
        DiceValue updatedValue = d1.getValue();
        assertEquals(rolled, updatedValue);
        
        rolled = d1.roll();
        updatedValue = d1.getValue();
        assertEquals(rolled, updatedValue);
        
    }    
}
